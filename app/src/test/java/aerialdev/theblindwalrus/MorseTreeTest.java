package aerialdev.theblindwalrus;

import junit.framework.TestCase;


public class MorseTreeTest extends TestCase {

    public void testDecodeTheTree() throws Exception {
        MorseTree build = MorseTree.Builder.build();
        String decode = build.decode("... --- ...");
        assertEquals("SOS", decode);
    }

    public void testTest(){
        String testString = "... -- ...";
        String[] split = testString.split(WalrusConstants.LETTER_BREAK_SYMBOL);
            System.out.println("------------------("+split.length+")");
        for(String str : split){
            System.out.println("> "+str);
        }
        assertEquals(split.length, 3);
    }
}