package aerialdev.theblindwalrus;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;


public class ProcessTaskTest extends TestCase {

    public void testProcess(){
        List<Measurement> measurementList = new ArrayList<Measurement>();

        measurementList.add(new Measurement(3812, 10));
        measurementList.add(new Measurement(4104, 10));
        measurementList.add(new Measurement(4397, 10));
        measurementList.add(new Measurement(4690, 10));
        measurementList.add(new Measurement(4983, 10));
        measurementList.add(new Measurement(5276, 10));
        measurementList.add(new Measurement(5569, 555));
        measurementList.add(new Measurement(5862, 140));
        measurementList.add(new Measurement(6155, 547));
        measurementList.add(new Measurement(6449, 247));
        measurementList.add(new Measurement(6741, 540));
        measurementList.add(new Measurement(7034, 10));
        measurementList.add(new Measurement(7330, 10));
        measurementList.add(new Measurement(7620, 10));
        measurementList.add(new Measurement(7913, 550));
        measurementList.add(new Measurement(8206, 552));
        measurementList.add(new Measurement(8499, 322));
        measurementList.add(new Measurement(8792, 10));
        measurementList.add(new Measurement(9085, 560));
        measurementList.add(new Measurement(9378, 555));
        measurementList.add(new Measurement(9671, 165));
        measurementList.add(new Measurement(9964, 10));
        measurementList.add(new Measurement(10257, 10));
        measurementList.add(new Measurement(10551, 150));
        measurementList.add(new Measurement(10843, 120));
        measurementList.add(new Measurement(11136, 182));
        measurementList.add(new Measurement(11429, 112));
        measurementList.add(new Measurement(11721, 220));
        measurementList.add(new Measurement(12014, 45));
        measurementList.add(new Measurement(12311, 10));
        measurementList.add(new Measurement(12601, 10));
        measurementList.add(new Measurement(12893, 10));
        measurementList.add(new Measurement(13187, 10));
        measurementList.add(new Measurement(13480, 10));
        measurementList.add(new Measurement(13772, 10));
        measurementList.add(new Measurement(14065, 10));
        measurementList.add(new Measurement(14359, 12));
        measurementList.add(new Measurement(14652, 12));

        ProcessTask processTask = new ProcessTask(new ViewInterface<String>() {
            @Override
            public void showInView(String[] data) {
                assertEquals(".-", data);
            }
        });
        String[] s = processTask.doInBackground(measurementList);
        assertEquals("AA", s);
    }
}