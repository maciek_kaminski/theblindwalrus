package aerialdev.theblindwalrus;

public class WalrusConstants {

    public static Double VALUE_DELTA_PERCENT_TOLERANCE = 0.10;
    public static Double VALUE_DROP_PERCENT_TOLERANCE = 0.20;
    public static Long PROCESS_TIME_DELTA = 2000l;
    public static Long SHORT_PULSE_TIME= 350l;
    public static Long LETTER_BREAK_LENGTH= (long)2.5*SHORT_PULSE_TIME;
    public static String LONG_SIGNAL_SYMBOL= "-";
    public static String SHORT_SIGNAL_SYMBOL= ".";
    public static String LETTER_BREAK_SYMBOL= " ";
    public static Long MINIMAL_BASE_DELTA= 35l;
}