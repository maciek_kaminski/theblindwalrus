package aerialdev.theblindwalrus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MorseTree{

    private Node root;

    private MorseTree() {
        root = new Node();
        root.string = null;
        root.morse = null;
    }

    public Node getRoot() {
        return root;
    }

    private void setRoot(Node root) {
        this.root = root;
    }

    static class Node {
        private String string;
        private String morse;
        private Node parent;

        private List<Node> children = new ArrayList<Node>();

        public List<Node> getChildren() {
            return children;
        }

        public Node getParent() {
            return parent;
        }

        public String getMorse() {
            return morse;
        }
        public String getString() {
            return string;
        }

        public void addChild(Node child){
            getChildren().add(child);
        }

        public void setString(String string) {
            this.string = string;
        }

        public void setMorse(String morse) {
            this.morse = morse;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public void setChildren(List<Node> children) {
            this.children = children;
        }

        public Node getChildWithCode(char oneChr) {
            for(Node child: getChildren()){
                if(child.getMorse().equals(Character.toString(oneChr))){
                    return child;
                }
            }
            return null;
        }
    }

    public String decode(String morse){
        StringBuilder result = new StringBuilder();
        String[] split = morse.split(WalrusConstants.LETTER_BREAK_SYMBOL);
        for(String letter : split){
            String recurrent = recurrent(letter, 0, getRoot());
            result.append(recurrent);
        }
        return result.toString();
    }

    private String recurrent(String morse, int index, Node parentNode){
        char[] chars = morse.toCharArray();
        char morseChar = chars[index];
        Node childWithCode = parentNode.getChildWithCode(morseChar);
        if(childWithCode!=null){
            if(index==chars.length-1) {
                return childWithCode.getString();
            }else {
                return recurrent(morse, ++index, childWithCode);
            }
        }
        return "";
    }

    public static class Builder {

        private static HashMap<String, String> transcode = new HashMap<String, String>();
        static {
            transcode.put("A",".-");
            transcode.put("B","-...");
            transcode.put("C","-.-.");
            transcode.put("D","-..");
            transcode.put("E",".");
            transcode.put("F","..-.");
            transcode.put("G","--.");
            transcode.put("H","....");
            transcode.put("I","..");
            transcode.put("J",".---");
            transcode.put("K","-.-");
            transcode.put("L",".-..");
            transcode.put("M","--");
            transcode.put("N","-.");
            transcode.put("O","---");
            transcode.put("P",".--.");
            transcode.put("Q","--.-");
            transcode.put("R",".-.");
            transcode.put("S","...");
            transcode.put("T","-");
            transcode.put("U","..-");
            transcode.put("V","...-");
            transcode.put("W",".--");
            transcode.put("X","-..-");
            transcode.put("Y","-.--");
            transcode.put("Z","--..");
            transcode.put("1",".----");
            transcode.put("2","..---");
            transcode.put("3","...--");
            transcode.put("4","....-");
            transcode.put("5",".....");
            transcode.put("6","-....");
            transcode.put("7","--...");
            transcode.put("8","---..");
            transcode.put("9","----.");
            transcode.put("0","-----");
        }

        public static MorseTree build(){
            MorseTree tree = new MorseTree();
            Node root = new Node();

            for(Map.Entry<String, String> entry : transcode.entrySet()){
                Node parent = root;
                char[] elements = entry.getValue().toCharArray();
                for( int i=0; i<elements.length; i++){
                    char oneChr = elements[i];
                    Node childWithCode = parent.getChildWithCode(oneChr);
                    if(childWithCode==null){
                        childWithCode = new Node();
                        childWithCode.setMorse(Character.toString(oneChr));
                        childWithCode.setParent(parent);
                        parent.addChild(childWithCode);
                    }
                    if(i==(elements.length-1)){
                        childWithCode.setString(entry.getKey());
                    }
                    parent = childWithCode;
                }
            }

            tree.setRoot(root);
            return tree;
        }

    }

}

