package aerialdev.theblindwalrus;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.chart.PointStyle;
import org.achartengine.model.XYMultipleSeriesDataset;
import org.achartengine.model.XYSeries;
import org.achartengine.renderer.XYMultipleSeriesRenderer;
import org.achartengine.renderer.XYSeriesRenderer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;


public class MainActivity extends Activity implements ViewInterface<String> {

    private Preview mPreview2;

    ProgressBar progressBar;
    TextView valueTxt;
    TextView morseValue;
    TextView decodedValue;

    LinearLayout chartLyt;
    XYMultipleSeriesDataset dataset;
    GraphicalView chartView;

    private Camera mCamera;
    private CameraPreview mPreview;

    FrameLayout preview;

    private float lastChangeValue = 0.0f;
    private float maxValue = 0.0f;
    private long startTime = 0;
    private long lastProcessTime = 0l;
    private List<Measurement> data = new LinkedList<Measurement>();
    private List<String> translations = new LinkedList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        valueTxt = (TextView) findViewById(R.id.value);
        morseValue = (TextView) findViewById(R.id.morseValue);
        decodedValue = (TextView) findViewById(R.id.decodedValue);
        chartLyt = (LinearLayout) findViewById(R.id.chart);
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        Sensor lightSensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);

        if (lightSensor == null) {
            Toast.makeText(this, "No Light Sensor! quit-", Toast.LENGTH_LONG).show();
        } else {
            //float max = lightSensor.getMaximumRange();
            //progressBar.setMax((int) max);
            startTime = System.currentTimeMillis();
            sensorManager.registerListener(lightSensorEventListener, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);
        }

        // Create an instance of Camera
        mCamera = getCameraInstance();

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(mPreview,0);

    }

    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    SensorEventListener lightSensorEventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
                processValue(event.values[0]);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    private void processValue(float value) {
        lastChangeValue = value;
        long now = System.currentTimeMillis();
        data.add(new Measurement(now, lastChangeValue));

        if(value>maxValue){
            maxValue=value;
        }

        Log.e("##################", (now - startTime) + ", " + lastChangeValue);
        valueTxt.setText(Float.toString(lastChangeValue));
        progressBar.setProgress((int) lastChangeValue);

        if (now - lastProcessTime > WalrusConstants.PROCESS_TIME_DELTA) {
            Log.e("$$$$$$$$$$", "Starting Task with data(" + data.size() + "): " + data);
            new ProcessTask(this).execute(new ArrayList<Measurement>(data));
            lastProcessTime = now;

        }

        if (dataset == null) {
            dataset = createDataset(data, startTime);
        } else {
            addToDataset(dataset, data);
        }
        if (chartView == null) {
            chartView = ChartFactory.getLineChartView(this,
                    dataset,
                    createRenderer(data.get(data.size() - 1).getTime() - startTime,
                            maxValue));
            //chartLyt.addView(chartView, 0);
            preview.addView(chartView, 1);
        } else {
             chartView.invalidate();
        }
    }

    private void addToDataset(XYMultipleSeriesDataset dataset, List<Measurement> data) {
        //hew was acutaly adding to that small dataset not multi
        XYSeries xySeries = dataset.getSeries()[0];
        dataset.removeSeries(xySeries);
        xySeries.clear();
        for (Measurement measurement : data) { //same data again?
            xySeries.add(measurement.getTime() - startTime, measurement.getValue());
        }
        dataset.addSeries(xySeries);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showInView(final String[] data) {
        if(data==null || data.length!=2){
            morseValue.setText("Result data ERROR");
        }
        final AlphaAnimation fadeIn = new AlphaAnimation(0.0f, 1.0f);
        final AlphaAnimation fadeOut = new AlphaAnimation(1.0f, 0.0f);
        fadeIn.setDuration(1000);
        fadeIn.setFillAfter(true);
        fadeOut.setDuration(1000);
        fadeOut.setFillAfter(true);
        //fadeOut.setStartOffset(4000 + fadeIn.getStartOffset());
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                morseValue.setText(data[0]);
                morseValue.startAnimation(fadeIn);
                decodedValue.setText(data[1]);
                decodedValue.startAnimation(fadeIn);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        morseValue.startAnimation(fadeOut);
        decodedValue.startAnimation(fadeOut);
    }

    private XYMultipleSeriesRenderer createRenderer(long maxX, float maxy) {
        // Now we create the renderer
        XYSeriesRenderer renderer = new XYSeriesRenderer();
        renderer.setLineWidth(8);
        renderer.setColor(Color.GREEN);
        // Include low and max value
        renderer.setDisplayBoundingPoints(true);
        // we add point markers
        renderer.setPointStyle(PointStyle.POINT);
        renderer.setPointStrokeWidth(8);
        renderer.setFillPoints(true);

        XYSeriesRenderer.FillOutsideLine.Type fillWhere = XYSeriesRenderer.FillOutsideLine.Type.BELOW;
        XYSeriesRenderer.FillOutsideLine fill = new XYSeriesRenderer.FillOutsideLine(fillWhere);
        fill.setColor(Color.GREEN);
        renderer.addFillOutsideLine(fill);

        XYMultipleSeriesRenderer mRenderer = new XYMultipleSeriesRenderer();
        mRenderer.addSeriesRenderer(renderer);
        // We want to avoid black border
        mRenderer.setMarginsColor(Color.argb(0x00, 0xff, 0x00, 0x00)); // transparent margins
        // Disable Pan on two axis
        mRenderer.setPanEnabled(false, false);
//        mRenderer.setYAxisMax(maxy*1.1);
//        mRenderer.setYAxisMin(0);
//        mRenderer.setXAxisMin(0);
//        mRenderer.setXAxisMax(maxX);
        mRenderer.setShowGrid(false);
        mRenderer.setZoomEnabled(false, false);
        mRenderer.setZoomButtonsVisible(false);
        mRenderer.setShowLegend(false);

        return mRenderer;
    }


    private XYMultipleSeriesDataset createDataset(List<Measurement> data, long startTime) {
        XYMultipleSeriesDataset dataset = new XYMultipleSeriesDataset();

        XYSeries series = new XYSeries("Lignt values");
        for (Measurement measurement : data) {
            series.add(measurement.getTime() - startTime, measurement.getValue());
        }

        dataset.addSeries(series);
        return dataset;
    }
}
