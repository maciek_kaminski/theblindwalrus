package aerialdev.theblindwalrus;


import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.io.IOException;
import java.util.List;

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "BW_CameraPreview";
    private SurfaceHolder mHolder;
    private Camera mCamera;

    public CameraPreview(Context context, Camera camera) {
        super(context);
        mCamera = camera;

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mHolder = getHolder();
        mHolder.addCallback(this);
        // deprecated setting, but required on Android versions prior to 3.0
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }


    public void surfaceCreated(SurfaceHolder holder) {
        Camera.Parameters param;
        param = mCamera.getParameters();

        mCamera.setDisplayOrientation(90);
        mCamera.setParameters(param);

        // The Surface has been created, now tell the camera where to draw the preview.
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting camera preview: " + e.getMessage());
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        // empty. Take care of releasing the Camera preview in your activity.
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        // If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.

        if (mHolder.getSurface() == null){
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e){
            // ignore: tried to stop a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            Camera.Parameters param;
            mCamera.setPreviewDisplay(mHolder);
            param = mCamera.getParameters();

            Camera.Size size = determinePreviewSize(true, w, h);
            adjustSurfaceLayoutSize(size, w, h);
            mCamera.setParameters(param);
            mCamera.setDisplayOrientation(90);
            mCamera.startPreview();

        } catch (Exception e){
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }
    }

    protected void adjustSurfaceLayoutSize(Camera.Size previewSize, int availableWidth, int availableHeight) {

        ViewGroup.LayoutParams layoutParams = this.getLayoutParams();
        layoutParams.width = previewSize.width;
        layoutParams.height = previewSize.height;
        this.setLayoutParams(layoutParams);
    }

    protected Camera.Size determinePreviewSize(boolean portrait, int reqWidth, int reqHeight) {

        int reqPreviewWidth = (int)(reqWidth * 1);
        int reqPreviewHeight = (int)(reqHeight * 1);
        // Adjust surface size with the closest aspect-ratio
//        float reqRatio = ((float) reqPreviewWidth) / reqPreviewHeight;
//        float curRatio, deltaRatio;
//        float deltaRatioMin = Float.MAX_VALUE;
//        Camera.Size retSize = null;
//        for (Camera.Size size :  mCamera.getParameters().getSupportedPreviewSizes()) {
//            curRatio = ((float) size.width) / size.height;
//            deltaRatio = Math.abs(reqRatio - curRatio);
//            if (deltaRatio < deltaRatioMin) {
//                deltaRatioMin = deltaRatio;
//                retSize = size;
//            }
//        }

        float  deltaRatioMin = Float.MAX_VALUE;
        float curDelta, deltaDelta;
        Camera.Size retSize = null;
        for (Camera.Size size :  mCamera.getParameters().getSupportedPreviewSizes()) {
            curDelta = Math.abs(reqPreviewWidth - size.width);
            if (curDelta < deltaRatioMin) {
                deltaRatioMin = curDelta;
                retSize = size;
            }
        }

        return retSize;
    }
}