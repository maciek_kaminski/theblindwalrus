package aerialdev.theblindwalrus;

public class Measurement {

    private long time;
    private float value;

    public Measurement(long time, float value) {
        this.time = time;
        this.value = value;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public float getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }


    public static long countTimeDelta(Measurement o1, Measurement o2){
        return o2.getTime() - o1.getTime();
    }

    public static float countValueDelta(Measurement o1, Measurement o2){
        return o2.getValue() - o1.getValue();
    }

    public boolean inAccuracyErrorTolerance(Measurement o1){
        return ( Math.abs(countValueDelta(o1, this)) - (getValue()*WalrusConstants.VALUE_DELTA_PERCENT_TOLERANCE) ) <= 0;
    }

    @Override
    public String toString() {
        return "["+time+"@"+value+"] ";
    }
}
