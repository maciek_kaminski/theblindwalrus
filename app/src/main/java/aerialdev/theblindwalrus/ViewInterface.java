package aerialdev.theblindwalrus;

public interface ViewInterface<T> {
    public void showInView(T[] data);
}
