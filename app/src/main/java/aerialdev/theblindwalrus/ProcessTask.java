package aerialdev.theblindwalrus;

import android.os.AsyncTask;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ProcessTask extends AsyncTask<List<Measurement>, String, String[]>{

    ViewInterface<String> callback;
    MorseTree tree = MorseTree.Builder.build();
    List<Measurement> measurements = Collections.emptyList();


    public ProcessTask(ViewInterface<String> callback) {
        this.callback = callback;
    }

    @Override
    protected String[] doInBackground(List<Measurement>... params) {
        measurements = params[0];
        float base = findLowestValue(measurements);
        StringBuilder sb = new StringBuilder();

        SignalCycleType lastMeasuredType = null;
        long signalCycleStartTime = 0;

        Measurement last = null;
        for (Measurement next : measurements) {
            boolean deltaBigEnough = isBaseDeltaBigEnough(next, base);
            boolean isDropBigEnough = !deltaBigEnough || isDropBigEnough(next, last);
            last = next;

            if (!deltaBigEnough || isDropBigEnough) { //base appeared
                if (lastMeasuredType != SignalCycleType.BASE) {
                    long timeDelta = next.getTime() - signalCycleStartTime;
                    sb.append(processLastSignalCycle(lastMeasuredType, timeDelta));

                    lastMeasuredType = SignalCycleType.BASE;
                    signalCycleStartTime = next.getTime();
                }
            } else {//reading appeared
                if (lastMeasuredType != SignalCycleType.READING) {
                    long timeDelta = next.getTime() - signalCycleStartTime;
                    sb.append(processLastSignalCycle(lastMeasuredType, timeDelta));

                    if (lastMeasuredType == null) {
                        lastMeasuredType = SignalCycleType.BASE;
                    } else {
                        lastMeasuredType = SignalCycleType.READING;
                    }
                    signalCycleStartTime = next.getTime();
                }
            }
        }
        //Log.e(">>>>>>>>>>>>", "#=" + measurements.size() + " > " + sb.toString());
        if(sb.toString().trim().isEmpty()){
            return new String[]{"", ""};
        }
        String decode = tree.decode(sb.toString().trim());
        //Log.e("<<<<<<<<<<<", decode);
        return new String[]{sb.toString(), decode};
    }

    private String processLastSignalCycle(SignalCycleType lastMeasuredType, long timeDelta) {
        if(lastMeasuredType==SignalCycleType.READING){
            if(timeDelta>WalrusConstants.SHORT_PULSE_TIME){
                return WalrusConstants.LONG_SIGNAL_SYMBOL;
            }else{
                return WalrusConstants.SHORT_SIGNAL_SYMBOL;
            }
        }else if(lastMeasuredType==SignalCycleType.BASE){
            if(timeDelta>WalrusConstants.LETTER_BREAK_LENGTH){
                return WalrusConstants.LETTER_BREAK_SYMBOL;
            }
        } else if(lastMeasuredType==null){
            return "";
        }
        return "";
    }


    private boolean isBaseDeltaBigEnough(Measurement measurement, float base) {
        double baseTolerance = (base!=0) ? (base * (1 + WalrusConstants.VALUE_DELTA_PERCENT_TOLERANCE)) : WalrusConstants.MINIMAL_BASE_DELTA;
        return (measurement.getValue() - baseTolerance) >= 0;
    }

    private boolean isDropBigEnough(Measurement newOne, Measurement last) {
        if(last==null){
            return false;
        }
        float delta = last.getValue() - newOne.getValue();

        return delta>0 && delta>=last.getValue()*WalrusConstants.VALUE_DROP_PERCENT_TOLERANCE;
    }

    private float findLowestValue(List<Measurement> measurements){
        Measurement min = Collections.min(measurements, new Comparator<Measurement>() {
            @Override
            public int compare(Measurement lhs, Measurement rhs) {
                return Float.valueOf(lhs.getValue()).compareTo(rhs.getValue());
            }
        });
        return min.getValue();
    }

    private float findMaxValue(List<Measurement> measurements){
        Measurement max = Collections.max(measurements, new Comparator<Measurement>() {
            @Override
            public int compare(Measurement lhs, Measurement rhs) {
                return Float.valueOf(lhs.getValue()).compareTo(rhs.getValue());
            }
        });
        return max.getValue();
    }


    @Override
    protected void onPostExecute(String[] s) {
        callback.showInView(s);
    }

    private enum SignalCycleType {
        BASE, READING;
    }
}
